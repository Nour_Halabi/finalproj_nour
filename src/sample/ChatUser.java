package sample;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.FileInputStream;

/**
 * Created by nourhalabi on 03/04/16.
 */
public class ChatUser {

    private ImageView imageView = null;
    private String name = "";
    private String message;

    public ChatUser(ImageView Imageview, String Name, String Message){
        this.imageView = Imageview;
        this.name = Name;
        this.message = Message;
    }


    public ImageView getImageView() {
        return imageView;
    }

    public void setIMageView(int num){
        if(num == 1){
            File file = new File("Icons/image1.png");
            String fileLocation = file.toURI().toString();
            Image image = new Image(fileLocation);
            this.imageView = new ImageView(image);
            this.imageView.setFitHeight(100);
            this.imageView.setFitWidth(100);
        }
        else if(num == 2){
            File file = new File("Icons/image2.png");
            String fileLocation = file.toURI().toString();
            Image image = new Image(fileLocation);
            this.imageView = new ImageView(image);
            this.imageView.setFitHeight(100);
            this.imageView.setFitWidth(100);
        }
        else if(num == 3){
            File file = new File("Icons/image3.png");
            String fileLocation = file.toURI().toString();
            Image image = new Image(fileLocation);
            this.imageView = new ImageView(image);
            this.imageView.setFitHeight(100);
            this.imageView.setFitWidth(100);
        }
        else if(num == 4){
            File file = new File("Icons/image4.png");
            String fileLocation = file.toURI().toString();
            Image image = new Image(fileLocation);
            this.imageView = new ImageView(image);
            this.imageView.setFitHeight(100);
            this.imageView.setFitWidth(100);
        }
        else if(num == 5){
            File file = new File("Icons/image5.png");
            String fileLocation = file.toURI().toString();
            Image image = new Image(fileLocation);
            this.imageView = new ImageView(image);
            this.imageView.setFitHeight(100);
            this.imageView.setFitWidth(100);
        }
        else if(num == 6){
            File file = new File("Icons/image6.png");
            String fileLocation = file.toURI().toString();
            Image image = new Image(fileLocation);
            this.imageView = new ImageView(image);
            this.imageView.setFitHeight(100);
            this.imageView.setFitWidth(100);
        }
        else if(num == 7){
            File file = new File("Icons/image7.png");
            String fileLocation = file.toURI().toString();
            Image image = new Image(fileLocation);
            this.imageView = new ImageView(image);
            this.imageView.setFitHeight(100);
            this.imageView.setFitWidth(100);
        }
        else if(num == 8){
            File file = new File("Icons/image8.png");
            String fileLocation = file.toURI().toString();
            Image image = new Image(fileLocation);
            this.imageView = new ImageView(image);
            this.imageView.setFitHeight(100);
            this.imageView.setFitWidth(100);
        }
        else if(num == 9){
            File file = new File("Icons/image9.png");
            String fileLocation = file.toURI().toString();
            Image image = new Image(fileLocation);
            this.imageView = new ImageView(image);
            this.imageView.setFitHeight(100);
            this.imageView.setFitWidth(100);
        }
        else{
            File file = new File("Icons/image10.png");
            String fileLocation = file.toURI().toString();
            Image image = new Image(fileLocation);
            this.imageView = new ImageView(image);
            this.imageView.setFitHeight(100);
            this.imageView.setFitWidth(100);
        }
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



}
