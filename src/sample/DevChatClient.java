package sample;

import javafx.beans.value.ObservableListValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class DevChatClient extends Frame {

    private Socket socket = null;
    private BufferedReader in = null;
    private PrintWriter networkOut = null;
    private BufferedReader networkIn = null;

    public static String SERVER_ADDRESS = "localhost";
    public static int SERVER_PORT = 8888;
    public String username = "";
    public int picIndex = -1;


    //

    public DevChatClient(int picIndex, String username) {
        this.username = username;
        this.picIndex = picIndex;

        try {
            socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
        } catch (UnknownHostException e) {
            System.err.println("Unknown host: " + SERVER_ADDRESS);
        } catch (IOException e) {
            System.err.println("IOException while connecting to server: " + SERVER_ADDRESS);
        }

        // open read/write connection
        try {
            networkOut = new PrintWriter(socket.getOutputStream(), true);
            networkIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            System.err.println("IOException while opening a read/write connection");
        }

        in = new BufferedReader(new InputStreamReader(System.in));

    }

    public void addMsg(String outMsg) {
        networkOut.println("ADDMSG " + picIndex + " " + username + " " + outMsg);
    }

    public ObservableList<ChatUser> retrieveMsg() {
        networkOut.println("GETMSGS");

        ObservableList<ChatUser> inChat = FXCollections.observableArrayList();

        String buffer;
        inChat.clear();

        try {

            String line = null;
            while ((line = networkIn.readLine()) != null) {
                System.out.println("Line: " + line);
                System.out.println("Index " + line.substring(0,1));
                System.out.println("Username " + line.substring(2, line.indexOf(" ", 2)));
                int MsgIndex = Integer.parseInt(line.substring(0,1));//ger.parseInt(line.substring(0, 1));
                String MsgUsername = line.substring(2, line.indexOf(" ", 2));
                String Msg = line.substring(line.indexOf(" ", 2) + 1);
                ChatUser chatUser = new ChatUser(null, MsgUsername, Msg);
                chatUser.setIMageView(MsgIndex);
                inChat.add(chatUser);
            }

        } catch (IOException e) {
            System.err.println("error reading from log");
        }

        return inChat;
    }


}
