package main.java.DevChatServer;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

public class DevChatServer {
    protected File logFile = null;
    protected Socket clientSocket = null;
    protected ServerSocket serverSocket = null;

    public static int SERVER_PORT = 8888;
    public static int MAX_CLIENTS = 25;

    public DevChatServer() {
        System.out.println("Dev Chat Server~~~");
        logFile = new File("log.txt");

        try {
            logFile.createNewFile();
        } catch (IOException e) {
            System.err.println("failed to create log file");
        }


        try {
            serverSocket = new ServerSocket(SERVER_PORT);
            System.out.println("Server Socket created");

            while (true) {
                System.out.println("Awaiting connection...");
                clientSocket = serverSocket.accept();
                System.out.println("A new client has connected.");

                if (Thread.activeCount() < MAX_CLIENTS) {
                    DevChatServerThread newThread = new DevChatServerThread(logFile, clientSocket);
                    newThread.start();
                }
            }
        } catch (IOException e) {
            System.err.println("IOException while creating server connection");
        }

        try {
            serverSocket.close();
            System.out.println("server socket closed successfully");
        } catch (IOException e) {
            System.err.println("IOException while closing server connection");
        }
    }

    public static void main(String[] args) {
        DevChatServer app = new DevChatServer();
    }
}