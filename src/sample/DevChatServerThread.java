package main.java.DevChatServer;

import java.io.*;
import java.net.*;

public class DevChatServerThread extends Thread {
    protected File logFile;
    protected Socket socket = null;
    protected PrintWriter out = null;
    protected BufferedReader in = null;

    public DevChatServerThread(File logFile, Socket socket) {
        super();
        this.logFile = logFile;
        this.socket = socket;

        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            System.err.println("IOException while opening a read/write connection");
        }
    }

    public void run() {
        // initialize interaction
        boolean endOfSession = false;
        while (!endOfSession) {
            endOfSession = processCommand();
        }
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected boolean processCommand() {
        String message = null;
        try {
            message = in.readLine();
        } catch (IOException e) {
            System.err.println("Error reading command from socket.");
            return true;
        }
        if (message == null) {
            return true;
        }

        // separate command and data
        String command = null;
        String data = null;
        if (message.indexOf(" ") > 0) {
            command = message.substring(0, message.indexOf(" "));
            data = message.substring(message.indexOf(" ") + 1, message.length());
        } else {
            command = message;
            data = null;
        }

        return processCommand(command, data);
    }

    protected boolean processCommand(String command, String data) {
        try {
            System.out.println(command);
            System.out.println(data);
            if(command.equals("DIR"))
            {
                //System.out.println("HELLO");
                dir();
                return false;
            }

            else if(command.equals("UPLOAD"))
            {
                downloadFromClient();
                return false;
            }
            else if(command.equals("DOWNLOAD"))
            {
                try {
                    uploadToClient();
                    return false;
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            if (command.equalsIgnoreCase("ADDMSG")) { // add msg
                BufferedWriter writer = new BufferedWriter(new FileWriter(logFile.getAbsolutePath(), true));
                writer.newLine();
                writer.append(data);
                writer.close();
                System.out.println("Message received.");
                return false;
            } else if (command.equalsIgnoreCase("GETMSGS")) { // get msgs
                String message = "";
                FileReader fileReader = new FileReader(logFile.getAbsolutePath());
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    message += line + "\n";
                }
                message = message.substring(0, message.length() - 1);
                System.out.println(message);
                out.println(message);
                return false;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    void dir() {        //Git all the files in the shared director and send their names back to the client.

        File file = new File("Shared/");
        System.out.println("1STUFF");

        if (file.isDirectory()) {

            //Get a list of files.
            File[] filesInDir = file.listFiles();

           // PrintWriter outstream = new PrintWriter(out,true);
            System.out.println("2WOW");

            String[] FileNames = new String[filesInDir.length];

            for (int i = 0; i < filesInDir.length; i++) {
                FileNames[i] = filesInDir[i].getName();
                out.println(FileNames[i]);
                System.out.println("3DAMN");
            }
            System.out.println("REALL");

        }
        try {
            out.close();
            in.close();
            socket.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }

    private void downloadFromClient(){      //Get the name of the file from the client and then create the file and fill it with contents from the client.
        try{
            String downloadedData = in.readLine();

            String newFileName = downloadedData;            //Incoming File Name.

            File outputFile = new File("Shared/" + newFileName);

            downloadedData = in.readLine();

            if (!outputFile.exists() || outputFile.canWrite()) {    //Create file in shared directory.
                PrintWriter fout = new PrintWriter(outputFile);
                while(downloadedData!=null)                         //File Contents.
                {   fout.println(downloadedData);
                    System.out.println(downloadedData);
                    downloadedData = in.readLine();

                }
                fout.close();
                out.close();
                in.close();
                socket.close();
            }



        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }

    public void uploadToClient() throws Exception{      //Get the file that the client wants and send the contents to them.

        String fileNameInput = in.readLine();
        File file = new File("Shared/" + fileNameInput);        //Get the file.

        BufferedReader transfer = new BufferedReader(new FileReader(file));     //Get the contents of the file.
        PrintWriter outstream = new PrintWriter(out,true);
        String line = transfer.readLine();
        while(line!=null)                                          //Send the contents of the file back to the client.
        {
            outstream.println(line);
            System.out.println(line);
            line=transfer.readLine();
        }
        outstream.flush();                                          //Clear the stream.

        outstream.close();                                        //Close ths stream.

        out.close();
        in.close();
        socket.close();




    }
}