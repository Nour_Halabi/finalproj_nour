package sample;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableListValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.*;
import java.net.Socket;

public class Main extends Application {
    BorderPane layout = new BorderPane();
    //Window 1 variables
    Button join_btn;
    TextField Name_text;
    int icon_num = 0;
    Button icon1, icon2, icon3, icon4, icon5, icon6, icon7, icon8, icon9, icon10 = null;
    //Window 2 variables
    TableView<ChatUser> msg_table;
    ChatUser user1 = null;
    TextField msg_text;

    double counter = 0.0;

    ObservableList<ChatUser> msgList;

    DevChatClient chatClient = null;

    //File Sharing
    private String currentlySelectedClient = "";    //String that captures which client file item is selected.
    private String currentlySelectedServer = "";    //String that captures which Server File item is selected.
    ObservableList<String> clientFileNames = FXCollections.observableArrayList("File1", "File2", "File3");  //List of Strings containing Client side Files.
    ObservableList<String> serverFileNames = FXCollections.observableArrayList("File1", "File2", "File3");  //List of string containing Server Side Files.

    ListView<String> clientList = new ListView<>(clientFileNames);    //Listview used for the Client's files.
    ListView<String> serverList = new ListView<>(serverFileNames);    //Listview used for the Server's Files.

    @Override
    public void start(Stage primaryStage) throws Exception{
         primaryStage.setTitle("Game Dev Chat");
       // primaryStage.initStyle(StageStyle.TRANSPARENT);



        createMainWindow();

        Scene scene = new Scene(layout, 800, 600);
        //scene.setFill(Color.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.show();


//        while(true)
//        {
//            if (chatClient != null) {
//                if (counter >= 1.0) {
//                    msgList = chatClient.retrieveMsg();
//                    counter = 0.0;
//                }
//                counter += 0.01;
//            }
//        }



    }

    private void createMainWindow(){

        //Scene 1
        join_btn = new Button("Join");
        Name_text = new TextField();
        Name_text.setPromptText("Enter your name");
        join_btn.setOnAction(this::handleButtonJoin);

        GridPane editArea2 = createIcons();

        //use vbox to allign button and textfield properly in the gridpane
        VBox vbox = new VBox();
        vbox.setSpacing(10.0);

        //_____create gridpane in order to customize placement of UI_____//
        GridPane editArea = new GridPane();
        editArea.setPadding(new Insets(10, 10, 150, 10));
        editArea.setVgap(10);
        editArea.setHgap(10);
        editArea.setAlignment(Pos.CENTER);
        join_btn.setAlignment(Pos.CENTER);
        Name_text.setAlignment(Pos.CENTER);

        vbox.setAlignment(Pos.CENTER);
        vbox.getChildren().addAll(Name_text,join_btn);

        editArea.add(vbox,0,2);

        layout.setCenter(editArea2);
        layout.setBottom(editArea);


    }

    //_____Creates the gridpane for the 2 icon rows_____//
    private GridPane createIcons(){
        //_____create a label for UI info_____//
        Label info_label = new Label("Please select an icon to use.");
        info_label.setStyle("-fx-font-family: Century Gothic;" +
                "-fx-font-size: 12px;" +
                "    -fx-font-weight: bold;" +
                "    -fx-text-fill: #333333;" +
                "    -fx-effect: dropshadow( gaussian , rgba(255,255,255,0.5) , 0,0,0,1 );");
        info_label.setAlignment(Pos.CENTER);
        info_label.setPadding(new Insets(10, 10, 10, 100));//TOP, RIGHT, BOTTOM, LEFT

        //_____create icons using buttons_____//
        icon1 = new Button();
        icon2 = new Button();
        icon3 = new Button();
        icon4 = new Button();
        icon5 = new Button();
        icon6 = new Button();
        icon7 = new Button();
        icon8 = new Button();
        icon9 = new Button();
        icon10 = new Button();

        //_____set function to handle if button was pressed_____//
        icon1.setOnAction(this::actionPerformed);
        icon2.setOnAction(this::actionPerformed);
        icon3.setOnAction(this::actionPerformed);
        icon4.setOnAction(this::actionPerformed);
        icon5.setOnAction(this::actionPerformed);
        icon6.setOnAction(this::actionPerformed);
        icon7.setOnAction(this::actionPerformed);
        icon8.setOnAction(this::actionPerformed);
        icon9.setOnAction(this::actionPerformed);
        icon10.setOnAction(this::actionPerformed);

        //Load in the images for the icons and
        //set them to the corresponding button
        File file = new File("Icons/image1.png");
        String fileLocation = file.toURI().toString();
        Image image = new Image(fileLocation);
        ImageView imageview1 = new ImageView(image);
        imageview1.setFitHeight(50);
        imageview1.setFitWidth(50);
        icon1.setGraphic(imageview1);

        file = new File("Icons/image2.png");
        fileLocation = file.toURI().toString();
        image = new Image(fileLocation);
        ImageView imageview2 = new ImageView(image);
        imageview2.setFitHeight(50);
        imageview2.setFitWidth(50);
        icon2.setGraphic(imageview2);

        file = new File("Icons/image3.png");
        fileLocation = file.toURI().toString();
        image = new Image(fileLocation);
        ImageView imageview3 = new ImageView(image);
        imageview3.setFitHeight(50);
        imageview3.setFitWidth(50);
        icon3.setGraphic(imageview3);

        file = new File("Icons/image4.png");
        fileLocation = file.toURI().toString();
        image = new Image(fileLocation);
        ImageView imageview4 = new ImageView(image);
        imageview4.setFitHeight(50);
        imageview4.setFitWidth(50);
        icon4.setGraphic(imageview4);

        file = new File("Icons/image5.png");
        fileLocation = file.toURI().toString();
        image = new Image(fileLocation);
        ImageView imageview5 = new ImageView(image);
        imageview5.setFitHeight(50);
        imageview5.setFitWidth(50);
        icon5.setGraphic(imageview5);

        file = new File("Icons/image6.png");
        fileLocation = file.toURI().toString();
        image = new Image(fileLocation);
        ImageView imageview6 = new ImageView(image);
        imageview6.setFitHeight(50);
        imageview6.setFitWidth(50);
        icon6.setGraphic(imageview6);

        file = new File("Icons/image7.png");
        fileLocation = file.toURI().toString();
        image = new Image(fileLocation);
        ImageView imageview7 = new ImageView(image);
        imageview7.setFitHeight(50);
        imageview7.setFitWidth(50);
        icon7.setGraphic(imageview7);

        file = new File("Icons/image8.png");
        fileLocation = file.toURI().toString();
        image = new Image(fileLocation);
        ImageView imageview8 = new ImageView(image);
        imageview8.setFitHeight(50);
        imageview8.setFitWidth(50);
        icon8.setGraphic(imageview8);

        file = new File("Icons/image9.png");
        fileLocation = file.toURI().toString();
        image = new Image(fileLocation);
        ImageView imageview9 = new ImageView(image);
        imageview9.setFitHeight(50);
        imageview9.setFitWidth(50);
        icon9.setGraphic(imageview9);

        file = new File("Icons/image10.png");
        fileLocation = file.toURI().toString();
        image = new Image(fileLocation);
        ImageView imageview10 = new ImageView(image);
        imageview10.setFitHeight(50);
        imageview10.setFitWidth(50);
        icon10.setGraphic(imageview10);

        //create HBox so elements allign horizontally
        HBox hbox1 = new HBox();
        HBox hbox2 = new HBox();
        HBox hbox3 = new HBox();
        hbox1.setSpacing(10.0);
        hbox2.setSpacing(10.0);
        hbox3.setSpacing(10.0);
        hbox1.setAlignment(Pos.CENTER);
        hbox2.setAlignment(Pos.CENTER);
        hbox3.setAlignment(Pos.CENTER);

        //Add the buttons of each row to the corresponding horizontal box
        hbox1.getChildren().addAll(icon1, icon2, icon3, icon4, icon5);
        hbox2.getChildren().addAll(icon6, icon7, icon8, icon9, icon10);
        hbox3.getChildren().addAll(info_label);

        GridPane editArea2 = new GridPane();
        editArea2.setPadding(new Insets(150, 10, 10, 10));//top, right, bottom, left
        editArea2.setVgap(10);
        editArea2.setHgap(10);
        editArea2.setAlignment(Pos.CENTER);

        editArea2.add(info_label, 0, 0);
        editArea2.add(hbox1, 0,2);
        editArea2.add(hbox2,0,4);

        return editArea2;

    }

    //_____Once join button is clicked it will update the window_____//
    private void handleButtonJoin(ActionEvent event){
        System.out.println("Join");
        //_____create user_____//
        String name = Name_text.getText().toString();
        user1 = new ChatUser(null, name, "");
        user1.setIMageView(icon_num);


        //once the user has decided on name and icon, data is stored to be sent over later
        chatClient = new DevChatClient(icon_num, name);

        //_____Change window to chat_____//
        Stage stage;
        stage = (Stage)join_btn.getScene().getWindow();

        // root = something
        TabPane tabpane = createChatWindow();
        //create a new scene with root and set the stage
        Scene scene = new Scene(tabpane, 800, 600);
        stage.setScene(scene);
        stage.show();

    }

    //_____Handles buttons clicked for icons and updates the icon #_____//
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == icon1) {  icon_num = 1; }
        else if (e.getSource() == icon2) {  icon_num = 2; }
        else if (e.getSource() == icon3) {  icon_num = 3; }
        else if (e.getSource() == icon4) {  icon_num = 4; }
        else if (e.getSource() == icon5) {  icon_num = 5; }
        else if (e.getSource() == icon6) {  icon_num = 6; }
        else if (e.getSource() == icon7) {  icon_num = 7; }
        else if (e.getSource() == icon8) {  icon_num = 8; }
        else if (e.getSource() == icon9) {  icon_num = 9; }
        else {  icon_num = 10; }

        System.out.println(icon_num);
    }


    //_____Returns a border pane of the chat window_____//
    private TabPane createChatWindow(){
        //_____TABPANE_____//
        TabPane tabpane = new TabPane();
        Tab tab1 = new Tab("Chat");
        Tab tab2 = new Tab("File Share");
        //customizing tabs and tabpane
        tabpane.setStyle(" -fx-tab-min-width:90px; -fx-background-color: seashell; ");
        tab1.setStyle("  -fx-background-insets: 0 1 0 1,0,0; -fx-background-color: salmon;" +
                "  -fx-alignment: CENTER;" +
                "    -fx-text-fill: #828282;" +
                "    -fx-font-size: 12px;" +
                "    -fx-font-weight: bold; -fx-font: Century Gothic;");
        tab2.setStyle("  -fx-background-insets: 0 1 0 1,0,0; -fx-background-color: rosybrown;" +
                "  -fx-alignment: CENTER;" +
                "    -fx-text-fill: #828282;" +
                "    -fx-font-size: 12px;" +
                "    -fx-font-weight: bold; -fx-font: Century Gothic;");


        //_____MESSAGE_TABLE_____//
        //Create message table
        msg_table = new TableView();
        msg_table.setItems(msgList);
        msg_table.setEditable(false);
        msg_table.setMinHeight(100);
        msg_table.setFixedCellSize(110);
        Label initial_label = new Label("Start chatting!");
        initial_label.setFont(new Font("Century Gothic", 24));
        initial_label.setTextFill(Color.WHITE);
        msg_table.setPlaceholder(initial_label);
        //Customizing the table
        msg_table.setStyle("-fx-background-color:cadetblue; -fx-font-size: 11pt;" +
                "    -fx-font-family: Century Gothic; -fx-text-fill: white;" +
                "    -fx-control-inner-background: #1d1d1d;" +
                "    -fx-background-color: teal;" +
                "    -fx-table-cell-border-color: black;" +
                "    -fx-table-header-border-color: black;");


        //create columns
        TableColumn<ChatUser, ImageView> icon_column = new TableColumn<>();
        icon_column.setMinWidth(110);
        icon_column.setStyle("-fx-background-color:black; -fx-border-width: 0 0 1 0;" +
                " -fx-border-color: black");
        icon_column.setCellValueFactory((new PropertyValueFactory<>("imageView")));

        TableColumn<ChatUser, String> name_column = new TableColumn<>();
        name_column.setMinWidth(75);
        name_column.setStyle("-fx-alignment: CENTER; -fx-background-color:teal; " +
                "-fx-font-weight:bold; -fx-border-width: 0 0 1 0;"+
                "-fx-border-color: black");
        name_column.setCellValueFactory((new PropertyValueFactory<>("name")));

        TableColumn<ChatUser, String> msg_column = new TableColumn<>();
        msg_column.setMinWidth(500);
        msg_column.setStyle("-fx-alignment: CENTER-LEFT; -fx-background-color:cadetblue;" +
                "    -fx-border-width: 0 0 1 0;" +
                "    -fx-border-color: black");
        msg_column.setCellValueFactory((new PropertyValueFactory<>("message")));

        msg_table.getColumns().addAll(icon_column, name_column, msg_column);

        //_____LAYOUT_____//
        BorderPane layout2 = new BorderPane();
        GridPane editArea = new GridPane();
        editArea.setPadding(new Insets(10, 10, 10, 10));
        editArea.setAlignment(Pos.CENTER);
        HBox hbox = new HBox();
        hbox.setSpacing(10.0);
        HBox hbox2 = new HBox();
        hbox2.setAlignment(Pos.CENTER);


        BorderPane FileLayout = new BorderPane();
        GridPane ButtonArea = new GridPane();
        ButtonArea.setPadding(new Insets(10, 10, 10, 10));
        ButtonArea.setVgap(5);
        ButtonArea.setHgap(5);

        msg_text = new TextField();
        Button send_btn = new Button("Send");
        send_btn.setOnAction(this::handleButtonSend);
        msg_text.setMinWidth(700);


        hbox.getChildren().addAll(msg_text,send_btn);
        hbox2.getChildren().addAll(msg_table);
        editArea.getChildren().addAll(hbox);

        layout2.setCenter(hbox2);
        layout2.setBottom(editArea);
        tab1.setContent(layout2);

        setUpFileList(ButtonArea);
        FileLayout.setRight(serverList);
        FileLayout.setLeft(clientList);
        FileLayout.setTop(ButtonArea);
        tab2.setContent(FileLayout);

        tabpane.getTabs().addAll(tab1, tab2);

        //TODO: JON create add contents for file sharing to tab2
        //TODO: JON up to you where you want to put the code
        //TODO:(maybe you can create a separate function and call on it here to add it to tab2)

        return tabpane;
    }
    public void refreshList(){      //Sort through Local File Directory and add the name of each file to the Client List.
        clientList.getItems().clear();
        File file = new File("Local/");
        if (file.isDirectory()) {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();
            for (int i = 0; i < filesInDir.length; i++) {
                clientList.getItems().add(i,filesInDir[i].getName());
            }

        }


    }
    public void setUpFileList(GridPane ButtonArea){

           /* create an edit form (for the bottom of the user interface) */


//Create the client list and add a listener for user clicks, storing the last item clicked on the client side.
        clientList.setItems(clientFileNames);
        clientList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                currentlySelectedClient = newValue;
            }
        });

        //Create the Server list and add a listener for user clicks, storing the last item clicked on the client side.
        serverList.setItems(serverFileNames);
        serverList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                System.out.println(newValue);
                currentlySelectedServer = newValue;
            }
        });

        refreshList();                                                          //Refresh the Client List for the first time.
        refreshServerList();                                                    //Refresh the Server List for the first time.
        Button downloadButton = new Button("Download");                         //Create the Download Button and add the event listener.
        downloadButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                try{
                     downloadFile(new File("Local/" + currentlySelectedServer)); //Call the download file function by passing the selected file.
                }
                catch(Exception except)
                {
                    except.printStackTrace();
                }
                refreshList();                                                   //Refresh the List

            }
        });
        ButtonArea.add(downloadButton, 0, 0);

        Button uploadButton = new Button("Upload");                               //Create the Upload button and attach a listener to it.
        uploadButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {

                try {
                       uploadFile(new File("Local/" + currentlySelectedClient));     //Call the upload function passing the selected file.
                } catch (Exception except)
                {
                    except.printStackTrace();
                }
                 refreshServerList();                                               //Refresh the server list.

            }
        });
        ButtonArea.add(uploadButton, 1, 0);


    }
    void refreshServerList(){      //Send the DIR command to the server, and receive an updated list of the shared folder contents.

        Socket socket;
        BufferedReader in;
        PrintWriter out;

        String hostName = "";

        int portNumber = 8888;

        try {
            // connect to the server (3-way connection establishment handshake)
            socket = new Socket(hostName, portNumber);

            // wrap the input streams into readers and writers
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());



            String request = "DIR";     //Command DIR.
            String delim = "\r\n";
            out.print(request  + delim + delim);
            out.flush();

            // read the response
            String response;


            //Clear the Server list before re-adding all the items to the list again.
            serverList.getItems().clear();
            while ((response = in.readLine()) != null) {        //Cycle through each line of names and add them.
                System.out.println(response);
                serverList.getItems().add(response);

            }
            System.out.println("Huh");

            // close the connection (3-way tear down handshake)
            out.close();
            in.close();
           socket.close();
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }

    private String getContentType(String filename) {

        return "text/plain";

    }

    public void uploadFile(File file) throws Exception{     //Upload file to server.
        Socket socket;
        BufferedReader in;
        PrintWriter out;
        String hostName = "";

        BufferedReader transfer = new BufferedReader(new FileReader(file));


        int portNumber = 8888;


        try {
            // connect to the server (3-way connection establishment handshake)
            socket = new Socket(hostName, portNumber);

            // wrap the input streams into readers and writers
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            out = new PrintWriter(socket.getOutputStream());
            String delim = "\r\n";
            out.print("UPLOAD" + delim);                //Send UPLOAD command.
            out.flush();
            out.print(file.getName() + delim);          //Send the file name.
            out.flush();
            String line = transfer.readLine();          //Parse the file and send the contents.
            while(line!=null)
            {
                out.println(line);
                System.out.println(line);
                line=transfer.readLine();
            }
            out.flush();



            // close the connection (3-way tear down handshake)
            out.close();
            in.close();
            socket.close();

        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        }


    }

    public void downloadFile(File file) throws Exception{
        Socket socket;
        BufferedReader in;
        PrintWriter out;
        String hostName = "";

        System.out.println(file.getName());



        int portNumber = 8888;


        try {
            // connect to the server (3-way connection establishment handshake)
            socket = new Socket(hostName, portNumber);

            // wrap the input streams into readers and writers
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());

            String delim = "\r\n";
            out.print("DOWNLOAD" + delim);                       //Send the DOWNLOAD command.
            out.flush();
            out.print(file.getName() + delim);                   //Send the name of the file to download.
            out.flush();



            String newFileName = file.getName();                  //Use the file name when creating the file.
            String line = "";
            line = in.readLine();

            File outputFile = new File("Local/" + newFileName);    //Create the file and fill it with contents being sent from the server.

            if (!outputFile.exists() || outputFile.canWrite()) {
                PrintWriter fout = new PrintWriter(outputFile);
                while(line!=null)
                {   fout.println(line);
                    System.out.println(line);
                    line = in.readLine();

                }
                fout.close();
            }

            // close the connection (3-way tear down handshake)
            out.close();
            in.close();
            socket.close();

        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        }


    }


    //_____Will update the table with new message, and send message to server_____//
    private void handleButtonSend(ActionEvent event){
        System.out.println("Send");
        String msg = msg_text.getText().toString();
        user1.setMessage(msg);
        user1.setIMageView(icon_num);
        msg_text.clear();
        //adds new message to the table
        //msg_table.getItems().add(new ChatUser(user1.getImageView(), user1.getName(), user1.getMessage()));

        //send the message to the server, it will combine the icon number, the user name and
        //the message into one packet to send over
        chatClient.addMsg(msg);
        msgList = chatClient.retrieveMsg();

        //TODO: ISHRAF you will send message to client here if you like
        //TODO: ISHRAF you need to send the icon num, name of user, and message.

  }

    public static void main(String[] args) {
        launch(args);
    }
}
